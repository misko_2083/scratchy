DESTDIR ?= /
PREFIX ?= /usr/local


define INSTALL_HELP=
You need to specify which version(s) to install:
  1. For QT:  make install-qt
  2. For GTK: make install-gtk
  3. For micro QT:  make install-mqt
  4. For micro GTK: make install-mgtk
  5. To install all versions: make install-all

Same applies to uninstallation:
  1. For QT:  make uninstall-qt
  2. For GTK: make uninstall-gtk
  3. For micro QT:  make uninstall-mqt
  4. For micro GTK: make uninstall-mgtk
  5. To uninstall all versions: make uninstall-all
endef


.PHONY: help
help:
	@: $(info $(INSTALL_HELP))

.PHONY: install
install: help

.PHONY: install-gtk
install-gtk:
	install -D -m 755 src/scratchy $(DESTDIR)/$(PREFIX)/bin/scratchy
	install -D -m 644 data/scratchy.desktop $(DESTDIR)/$(PREFIX)/share/applications/scratchy.desktop

.PHONY: install-mgtk
install-mgtk:
	install -D -m 755 src/scratchy-micro $(DESTDIR)/$(PREFIX)/bin/scratchy-micro
	install -D -m 644 data/scratchy-micro.desktop $(DESTDIR)/$(PREFIX)/share/applications/scratchy-micro.desktop

.PHONY: install-qt
install-qt:
	install -D -m 755 src/scratchy-qt $(DESTDIR)/$(PREFIX)/bin/scratchy-qt
	install -D -m 644 data/scratchy-qt.desktop $(DESTDIR)/$(PREFIX)/share/applications/scratchy-qt.desktop

.PHONY: install-mqt
install-mqt:
	install -D -m 755 src/scratchy-qt-micro $(DESTDIR)/$(PREFIX)/bin/scratchy-qt-micro
	install -D -m 644 data/scratchy-qt-micro.desktop $(DESTDIR)/$(PREFIX)/share/applications/scratchy-qt-micro.desktop

.PHONY: install-all
install-all: install-gtk install-mgtk install-qt install-mqt


.PHONY: uninstall-gtk
uninstall-gtk:
	rm -f $(DESTDIR)/$(PREFIX)/bin/scratchy
	rm -f $(DESTDIR)/$(PREFIX)/share/applications/scratchy.desktop

.PHONY: uninstall-mgtk
uninstall-mgtk:
	rm -f $(DESTDIR)/$(PREFIX)/bin/scratchy-micro
	rm -f $(DESTDIR)/$(PREFIX)/share/applications/scratchy-micro.desktop

.PHONY: uninstall-qt
uninstall-qt:
	rm -f $(DESTDIR)/$(PREFIX)/bin/scratchy-qt
	rm -f $(DESTDIR)/$(PREFIX)/share/applications/scratchy-qt.desktop

.PHONY: uninstall-mqt
uninstall-mqt:
	rm -f $(DESTDIR)/$(PREFIX)/bin/scratchy-qt-micro
	rm -f $(DESTDIR)/$(PREFIX)/share/applications/scratchy-qt-micro.desktop

.PHONY: uninstall-all
uninstall-all: uninstall-gtk uninstall-mgtk uninstall-qt uninstall-mqt

.PHONY: uninstall
uninstall: help
