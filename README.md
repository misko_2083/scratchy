# Scratchy

Simple program for taking quick notes.  

---------------

Contents:
- [General Info](#general-info)
- [Screenshot](#scratchshot)
- [Dependencies](#dependencies)
- [Installation](#installation)
- [Keyboard shortcuts](#keyboard-shortcuts)
- [Configuration](#configuration)

---------------

## General Info:

Scratchy comes in two variants:

1. **Normal:**
    - Text pad
    - Save functionality
    - Additional buttons in the UI
    - Ability to keep the window on top
2. **Micro**:
    - Just a text pad

**Both variants:**
  - Have [keyboard shortcuts](#keyboard-shortcuts)
  - Can toggle text wrapping
  - Come in GTK3 and QT5 toolkits
  
"Undo/Redo" is present natively in QT5, while GTK version requires  
GtkSourceView to be (optionally) installed.  
Thus, if "Undo/Redo" functionality is desired but it doesn't work,  
make sure that GtkSourceView is installed (see [Dependencies](#dependencies))

  
---------------

- **Many thanks** to "tuerda" from Manjaro Forums for the [inspiration](https://forum.manjaro.org/t/recommend-the-simplest-text-editor-you-can-think-of/21707),  
without which Scratchy would not exist!  
  
---------------
  
## Scratchshot:  



![group-scratchshot](screenshots/group-scratchshot.png).  
  
---------------
  
## Dependencies:

GTK3 or QT5 with respective Python bindings and Python (2 or 3).  
- Debian/Ubuntu:
    - For QT: `sudo apt install python-pyqt5`
    - For GTK: `sudo apt install python-gi libgtk-3-0`
- Arch/Manjaro:
    - For QT: `sudo pacman -S python-pyqt5`
    - For GTK: `sudo pacman -S python-gobject gtk3`

Optionally, install GtkSourceView to enable "Undo/Redo" in GTK version:  
  - Arch/Manjaro: `sudo pacman -S gtksourceview3`  
  - Debian/Ubuntu: `sudo apt install libgtksourceview-3.0-1`

---------------

## Installation:

Clone:   
`git clone https://gitlab.com/Cofi/scratchy.git && cd scratchy`  
And install the prefered version using:  
`make install-version`  
( See `make help` for more details )  
  
If you prefer packages you can build them for Debian and Arch.  
Debian:  
`dpkg-buildpackage -uc -us`  
  
Arch:  
`cd PKGBUILD && makepkg`

---------------

## Keyboard shortcuts:

**Ctrl+Q**: Quit  
**Ctrl+S**: Save  
**Ctrl+R**: Clear text  
**Ctrl+T**: Toggle "Window on top" state  
**Ctrl+W**: Toggle text wrapping (on by default)

---------------

## Configuration:

To keep things as simple as possible, Scratchy doesn't have a config file.    
So if you wish to modify it, you'll need to edit the source directly, which  
should be self-explanatory for the most part.  

I'll just note the option to issue a warning on quit, present in normal versions  
can be disabled by changing:  
`warn_on_quit = True` to `warn_on_quit = False`

---------------
---------------

## TODO:

See issues with "TODO" label [here.](https://gitlab.com/Cofi/scratchy/issues?label_name%5B%5D=TODO)

---------------

[↑ back to top ↑](#scratchy)